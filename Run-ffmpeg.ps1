
function Run-ffmpeg() {
    param (
        [parameter(Position=0,Mandatory=$true)]
        [string]$m3u8,
        [parameter(Position=1,Mandatory=$false)]
        [string]$flags
    )
    
    #Author: rabel001
    #Date: 2/10/2020
    #Examples:
    #Minimal command line
    #Run-ffmpeg -m3u8 "https://url.to.m3u8"
    #Command line with ffmpeg flags
    #Run-ffmpeg -m3u8 "https://url.to.m3u8" -flags "-headers `"Referrer: https://referrer.web.site`" -orwhateverflag"

    $ffmpegid = $null;
    $duration = $null;
    $FFMPEG = "ffmpeg.exe";

    $ffmatCode = @"
    using System;
    public class ffmat {
        public static String FormatFileSize(Double fileSize) {
            if(fileSize < 0) {
                throw new ArgumentOutOfRangeException("fileSize");
            } else if(fileSize >= 1024 * 1024 * 1024) {
                return string.Format("{0:########0.00} GB", ((Double)fileSize) / (1024 * 1024 * 1024));
            } else if(fileSize >= 1024 * 1024) {
                return string.Format("{0:####0.00} MB", ((Double)fileSize) / (1024 * 1024));
            } else if(fileSize >= 1024) {
                return string.Format("{0:####0.00} KB", ((Double)fileSize) / 1024);
            } else {
                return string.Format("{0} bytes", fileSize);
            }
        }
        private static int changeProgress(string duration, string downloaded)
        {
            Double All = Convert.ToDouble(Convert.ToDouble(duration.Substring(0, 2)) * 60 * 60 +
                Convert.ToDouble(duration.Substring(3, 2)) * 60 +
			    Convert.ToDouble(duration.Substring(6, 2)) +
			    Convert.ToDouble(duration.Substring(9, 2)) / 100);

            Double Downloaded = Convert.ToDouble(Convert.ToDouble(downloaded.Substring(0, 2)) * 60 * 60 +
                Convert.ToDouble(downloaded.Substring(3, 2)) * 60 +
                Convert.ToDouble(downloaded.Substring(6, 2)) +
                Convert.ToDouble(downloaded.Substring(9, 2)) / 100);

            if(All == 0) All = 1;
            Double Progress = (Downloaded / All) * 100;
            if(Progress > 100) Progress = 100;
            if(Progress < 0) Progress = 0;
            return Convert.ToInt32(Progress);
        }
    }
"@;
    try {
        Add-Type $ffmatCode;
    } catch {}


    function SetFileLocation() {
        $fileName = $null;
        [void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms");
        $saveDialog = [System.Windows.Forms.SaveFileDialog]::new();
        $saveDialog.Filter = "mp4 (*.mp4)|*.mp4|webm (*.webm)|*.webm|flv (*.flv)|*.flv|mkv (*.mkv)|*.mkv|ts (*.ts)|*.ts|All Files (*.*)|*.*";
        $saveDialog.DefaultExt = "mp4";
        $userRoot = [System.Environment]::GetEnvironmentVariable("USERPROFILE");
        $downloadFolder = [System.IO.Path]::Combine($userRoot, "Downloads");
        if([System.IO.Directory]::Exists($downloadFolder)) {
            $saveDialog.InitialDirectory = $downloadFolder;
        } else {
            $saveDialog.InitialDirectory = [System.Environment+SpecialFolder]::MyDocuments;
        }
        [System.Windows.Forms.DialogResult]$results = $saveDialog.ShowDialog();
        if($results -eq [System.Windows.Forms.DialogResult]::OK) {
            $fileName = $saveDialog.FileName;
        }
        return $fileName;
    }

    function FormatSize($fsize) {
        if($sizeDB1 -ge (1024 * 1024 * 1024)) {
            return [String]::Format("{0:########0.00} GB", (([double]$fsize) / (1024 * 1024 * 1024)));
        } elseif($fsize -ge (1024 * 1024)) {
            return [String]::Format("{0:####0.00} MB", (([double]$fsize) / (1024 * 1024)));
        } elseif($fsize -ge (1024)) {
            return [String]::Format("{0:####0.00} KB", (([Double]$fsize) / 1024));
        } else {
            return [String]::Format("{0} bytes", $fsize);
        }
    }

    $mediaFile = SetFileLocation;
    if([String]::IsNullOrEmpty($mediaFile)) {
        Write-Host "`nNo file has been set. Try again.`n" -ForegroundColor Yellow;
        return;
    } else {
        Write-Host "`n`tSaving to $($mediaFile)`n" -ForegroundColor Cyan;
    }

    #$response = Read-Host -Prompt "`n`tFile name?`n`t";
    $cmdProcess = [System.Diagnostics.Process]::new();
    $cmdProcess.StartInfo.FileName = $FFMPEG;
    $cmdText = [string]::Format("-threads 0 -i `"{0}`" -c copy -y -bsf:a aac_adtstoasc -movflags +faststart `"{1}`" {2}", $m3u8, $mediaFile, $flags);
    $cmdProcess.StartInfo.Arguments = $cmdText;
    
    $cmdProcess.StartInfo.CreateNoWindow = $true;
    $cmdProcess.StartInfo.UseShellExecute = $false;
    $cmdProcess.StartInfo.RedirectStandardInput = $true;
    $cmdProcess.StartInfo.RedirectStandardOutput = $true;
    $cmdProcess.StartInfo.RedirectStandardError = $true;
    $cmdProcess.EnableRaisingEvents = $true;
    $scripBlock = 
    {
        if (-not [String]::IsNullOrEmpty($EventArgs.Data))
        {
            $Event.MessageData.AppendLine($Event.SourceEventArgs.Data);
            Write-Host $Event.SourceEventArgs.Data;
            $dur = [System.Text.RegularExpressions.Regex]::new("Duration: (\d\d[.:]){3}\d\d", [System.Text.RegularExpressions.RegexOptions]::Compiled -bor [System.Text.RegularExpressions.RegexOptions]::Singleline);
            if($dur.Match($Event.SourceEventArgs.Data).Value) {
                $duration = $dur.Match($Event.SourceEventArgs.Data).Value.Replace("Duration: ", "");
            }
            $rex = [System.Text.RegularExpressions.Regex]::new("(\d\d[.:]){3}\d\d", [System.Text.RegularExpressions.RegexOptions]::Compiled -bor [System.Text.RegularExpressions.RegexOptions]::Singleline);
            $time = $rex.Match($Event.SourceEventArgs.Data).Value;
            $size = [System.Text.RegularExpressions.Regex]::new("[1-9][0-9]{0,}kB time", [System.Text.RegularExpressions.RegexOptions]::Compiled -bor [System.Text.RegularExpressions.RegexOptions]::Singleline);
            $sizeDB = $size.Match($Event.SourceEventArgs.Data).Value;

            if($time -and $sizeDB) {
                $sizeDB1 = [System.Convert]::ToDouble($sizeDB.Replace("kB time", "")) * 1024;
                if($sizeDB1) {
                    $sizeDB2 = [ffmat]::FormatFileSize($sizeDB1);
                    #$sizeDB2 = FormatSize $sizeDB1;
                    if($sizeDB2) {
                        Write-Host "Duration: $($duration), Time: $($time), Size: $($sizeDB2)";
                        $all = [System.Convert]::ToDouble(
                        [System.Convert]::ToDouble($duration.Substring(0, 2)) * 60 * 60 +
                        [System.Convert]::ToDouble($duration.Substring(3, 2)) * 60 +
                        [System.Convert]::ToDouble($duration.Substring(6, 2)) +
                        [System.Convert]::ToDouble($duration.Substring(9, 2)) / 100
                        );
                        $downloaded = [System.Convert]::ToDouble(
                        [System.Convert]::ToDouble($time.Substring(0, 2)) * 60 * 60 +
                        [System.Convert]::ToDouble($time.Substring(3, 2)) * 60 +
                        [System.Convert]::ToDouble($time.Substring(6, 2)) +
                        [System.Convert]::ToDouble($time.Substring(9, 2)) / 100
                        );
                        #Write-Host "All: $($all), Downloaded: $($downloaded)";
                        if($all -eq 0) {
	                        $all = 1;
                        }
                        [Double]$progress = ($downloaded / $all) * 100;
                        if($progress -gt 100) { 
	                        $progress = 100;
                        }
                        if($progress -le 0) {
	                        $progress = 0;
                        }
                        $n = [System.Convert]::ToInt32($progress);
                        Write-Progress -Activity "Downloaded: $($sizeDB2) | (Press `"Esc`" to Exit)" -Status "$n% Complete:" -PercentComplete $n;
                    }
                }
            }
        }
    }
    $stdSb = New-Object -TypeName System.Text.StringBuilder;
    $errorSb = New-Object -TypeName System.Text.StringBuilder;
    $exitSb = New-Object -TypeName System.Text.StringBuilder;


    $stdEvent = Register-ObjectEvent -InputObject $cmdProcess -EventName OutputDataReceived -Action $scripBlock -MessageData $stdSb;
    $errorEvent = Register-ObjectEvent -InputObject $cmdProcess -EventName ErrorDataReceived -Action $scripBlock -MessageData $errorSb;
    #$something = Register-ObjectEvent -InputObject $cmdProcess -EventName Exited -Action $scripBlock -MessageData $exitSb;

    [void]$cmdProcess.Start();
    #Clear-Host;
    [System.IO.StreamWriter]$stream = $cmdProcess.StandardInput;
    $ffmpegid = $cmdProcess.Id;
    $cmdProcess.BeginOutputReadLine();
    $cmdProcess.BeginErrorReadLine();
    $continue = $true;
    $cont = $true;
    $ffmpegSessionEnd = {
        #$Event.MessageData.SetResult($true)
        #if (-not [String]::IsNullOrEmpty($EventArgs.Data))
        #{
        #    $Event.MessageData.AppendLine($Event.SourceEventArgs.Data);
        #}
        Write-Progress -Activity "Download in Progress (Press F12 to Exit)" -Status "Ready" -Completed;
        $a = "`n" + "#"*56;
        $b = "#" + " "*18 + "Download Finished!" + " "*18 + "#";
        $c = "#"*56 + "`n";
        Write-Host $a -ForegroundColor Cyan;
        Write-Host $b -ForegroundColor Cyan;
        Write-Host $c -ForegroundColor Cyan;
        Unregister-Event -SourceIdentifier ExitAction;
        $cont = $false;
        $cont;
    }
    #$ffmpegSession = Get-Process -Name ffmpeg;
    $cont = Register-ObjectEvent -InputObject $cmdProcess -EventName Exited -Action $ffmpegSessionEnd -SourceIdentifier ExitAction -MessageData $exitSb;
    Write-Host "`n`tPress the `"Esc`" button to exit.`n" -ForegroundColor Yellow;
    while($cont)
    {
        if($cmdProcess.HasExited) {
            $cont = $false;
        }
        if ([System.Console]::KeyAvailable)
        {
            $x = [System.Console]::ReadKey();
            switch ($x.key)
            {
                Escape {
                    #& taskkill /PID $ffmpegid /F
                    $stream.WriteLine("q");
                    $cmdProcess.WaitForExit();
                    #$exf = " "*15 + "ffmpeg was forced to quit.`n";
                    Write-Host "ffmpeg was forced to quit.`n" -ForegroundColor Yellow;
                    #Write-Host $exf -ForegroundColor Yellow;
                    $cont = $false;
                }
                default {
                    Write-Host "`nKey pressed: $($x.key)" -ForegroundColor Yellow;
                    Write-Host "Press the `"Esc`" button to exit.`n" -ForegroundColor Yellow;
                }
            }
        }
    }
}